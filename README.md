ElectronicItem
==============

Implements assignment below:

Using the code given in Original final version.php, create each type of electronic as classes.
Every ElectronicItem must have a function called maxExtras that limits the number of extras an electronic item can have.
The extras are a list of electronic items that are attached to another electronic item to complement it.

* The console can have a maximum of 4 extras
* The television has no maximum extras
* The microwave can't have any extras
* The controller can't have any extras

Create a scenario where a person would buy

A console, 2 televisions with different prices and a microwave

The console's and television's have extras and they are controllers.
The console has 2 remote controllers and 2 wired controllers.
The TV 1 has 2 remote controllers and the TV 2 has 1 remote controller.

Sort the items by price and output the total pricing.

That person's friend saw him with his new purchase and asked him how much the console and its controllers had cost him. Give him the answer.


System requirements
===================

 PHP >= 5.5

Changes to Original final version.php
====================================

1. Added missing type of "controller" to ElectronicItem class
2. Removed unused $type parameters of getSortedItems function in ElectronicItems class
3. Changed scope of $items to protected in ElectronicItems class, in order to access it in sub class
4. Fixed non working getItemsByType in ElectronicItems class
5. Fixed getSortedItems function in ElectronicItems class to work correctly on input that includes the items at the same price

Run instructions
=================

Include Solution.php to any PHP file and get an access to the following variables:

1. $total - total pricing based on the task scenario
2. $sortedItems - sorted items as requested by the task
3. $consolePrice - answer to the person's friend question from task
