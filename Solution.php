<?php

namespace Solution;

require_once('Original final version.php');

/**
 * Class MyElectronicItems
 * @package Solution
 */
class MyElectronicItems extends \ElectronicItems
{
    public function getPrice() {
        $total = 0;
        foreach($this->items as $item) {
            $total += $item->getPrice();
        }
        return $total;
    }
}

/**
 * Class MyElectronicItem
 * @package Solution
 */
abstract class MyElectronicItem extends \ElectronicItem
{

    /**
     * Collection of extra MyElectronicItems
     * @var
     */
    protected $extras;
    /**
     * Maximum allowed extra items
     * @var
     */
    protected $maxExtras;

    /**
     * Add extra items and update the item price to include the price of the extra items
     * @param ElectronicItem $item
     */
    public function add(MyElectronicItems $items)
    {
        if (count($this->items) <= $this->maxExtras) {
            $this->extras = $items;
            $this->setPrice($this->getPrice() + $items->getPrice());
        }
    }
}

/**
 * Class Console
 * @package Solution
 */
class Console extends MyElectronicItem
{
    /**
     *
     */
    public function __construct()
    {
        $this->setType('console');
        $this->maxExtras = 4;
    }
}

/**
 * Class Television
 * @package Solution
 */
class Television extends MyElectronicItem
{
    /**
     *
     */
    public function __construct()
    {
        $this->setType('television');
        $this->maxExtras = PHP_INT_MAX;
    }
}

/**
 * Class Microwave
 * @package Solution
 */
class Microwave extends MyElectronicItem
{
    /**
     *
     */
    public function __construct()
    {
        $this->setType('microwave');
        $this->maxExtras = 0;
    }
}

/**
 * Class Controller
 * @package Solution
 */
class Controller extends MyElectronicItem
{
    /**
     *
     */
    public function __construct()
    {
        $this->setType('controller');
        $this->maxExtras = 0;
    }
}

/**
 * Class Order
 * @package Solution
 */
class Order
{
    /**
     * @var MyElectronicItems
     */
    private $electronicItems;
    /**
     * @var
     */
    private $orderedItems;
    /**
     * @var
     */
    private $total;

    /**
     * @param MyElectronicItems $electronicItems
     */
    public function __construct(MyElectronicItems $electronicItems)
    {
        $this->electronicItems = $electronicItems;
    }

    /**
     * Lazy calculation of order total
     * @return float
     */
    public function getTotal()
    {
        if (!isset($this->total)) {
            $this->total = $this->electronicItems->getPrice();
        }
        return $this->total;
    }

    /**
     * @return mixed
     */
    public function getSortedItems()
    {
        if (!isset($this->orderedItems)) {
            $this->orderedItems = $this->electronicItems->getSortedItems();
        }

        return $this->orderedItems;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getItemsByType($type)
    {
        return $this->electronicItems->getItemsByType($type);
    }
}

//Scenario
$console = new Console();
$console->setPrice(250.00);
$console->add(new MyElectronicItems(array(
            (new Controller())->setWired(true)->setPrice(0),
            (new Controller())->setWired(true)->setPrice(0),
            (new Controller())->setWired(false)->setPrice(50.00),
            (new Controller())->setWired(false)->setPrice(50.00)
        )));

$tv1 = new Television();
$tv1->setPrice(1000.00);
$tv1->add(new MyElectronicItems(array(
            (new Controller())->setWired(false)->setPrice(50.00),
            (new Controller())->setWired(false)->setPrice(50.00)
        )));

$tv2 = new Television();
$tv2->setPrice(1500.00);
$tv2->add(new MyElectronicItems(array(
            (new Controller())->setWired(false)->setPrice(50.00)
        )));

$microwave = new Microwave();
$microwave->setPrice(350.00);

$items = new MyElectronicItems(array($console, $tv1, $tv2, $microwave));
$order = new Order($items);

//Output of the scenario
$total = $order->getTotal();
$sortedItems = $order->getSortedItems();
//Assuming that in the real scenario the items are available only in scope of the order
//Otherwise $console->getPrice() can be called
$consolePrice = $order->getItemsByType("console")[0]->getPrice();









